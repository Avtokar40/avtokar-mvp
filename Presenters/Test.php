<?php
/**
 * Created by PhpStorm.
 * User: vasil
 * Date: 04.04.2019
 * Time: 16:25
 */

namespace app\presenter;

use avtokar\lightmvc\Presenter;

class Test extends Presenter
{
    protected $dependences = ['auth'=>['login']];
}