<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


defined('DS') or define('DS', DIRECTORY_SEPARATOR);
defined('ROOT_DIR') or define('ROOT_DIR', __DIR__ . DS);

defined('VENDOR_DIR') or define('VENDOR_DIR', ROOT_DIR . 'vendor' . DS);
defined('APP_DIR') or define('APP_DIR', ROOT_DIR . 'app' . DS);

require VENDOR_DIR . "autoload.php";
$config = require APP_DIR . 'config.php';

\avtokar\lightmvc\Application::start($config);

\avtokar\lightmvc\Core::route();
