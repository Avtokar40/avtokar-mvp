<?php

namespace avtokar;

class Component
{

	/**
	 * class Имя класса
	 *
	 * @return string
	 */
	public static function _class()
	{
		$classPath = explode('\\', get_called_class());
		return ucfirst(strtolower(end($classPath)));
	}

	public function __get($name)
	{
		$getter = 'get' . ucfirst($name);
		if (method_exists($this, $getter)) {
			return $this->$getter();
		} elseif (method_exists($this, 'set' . $name)) {
			return null;
			throw new \Exception('Getting write-only property: ' . get_class($this) . '::' . $name);
		}
		return null;
		throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
	}

	public function __set($name, $value)
	{
		$setter = 'set' . ucfirst($name);
		if (method_exists($this, $setter)) {
			$this->$setter($value);
		} elseif (method_exists($this, 'get' . $name)) {
			throw new \Exception('Setting read-only property: ' . get_class($this) . '::' . $name);
		} else {
			throw new \Exception('Setting unknown property: ' . get_class($this) . '::' . $name);
		}
	}

    public function __isset($name)
    {
        // TODO: Implement __isset() method.
    }

    public static function __callStatic($name, $arguments)
    {
        $z=1;
    }
}

function themes(){
    $themes=[];
    $path = ROOT_DIR.'Layouts';
    if ($handle = opendir($path)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != ".." && is_dir($path.DS.$entry)) {
                $themes[$entry] = $entry;
            }
        }
        closedir($handle);
    }
    return $themes;
}
//todo отдача JS и CSS по имени файла и темы. Поменять логику и вызов на массив тем
function returnContent($name, $theme, $default){
    $path = ROOT_DIR.'Layouts'.DS;
    if( preg_match('#\.css#',$name)){
        header('Content-type: text/css;');
    } else {
        header("Content-type: application/x-javascript");
    }
    if (file_exists($path.$default.DS.$name) && filesize($path.$default.DS.$name)){
        echo file_get_contents($path.$default.DS.$name);
        echo "\n";
    };
    if (file_exists($path.$theme.DS.$name) && filesize($path.$theme.DS.$name)){
        echo file_get_contents($path.$theme.DS.$name);
    };
}