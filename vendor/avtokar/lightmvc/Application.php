<?php

namespace avtokar\lightmvc;

use app\model;
use avtokar\Component;
use function avtokar\themes;

class Application extends Component
{
    /** @var Application */
    private static $__instance;
    /** @var Db */
    private $_db = null;
    /** @var null|object */
    private $_config = null;
    /** @var null|object */
    private $_post;
    /** @var null|object */
    private $_get;
    /** @var null|Session */
    private $_session;
    /** @var null|model\Users */
    private $_user;
    /** @var bool */
    private $_ajax = false;
    /** @var PresentersList */
    private $_presenters = null;

    private $_themes = [];
    private $_layout = 'default';
    private $_default_layout = null;

    private function __construct($config)
    {
        $__get = [];
        $__post = [];

        if (!empty($config)) {
            $this->_config = (object)$config;
        };

        $__post = $this->escape($_POST);
        $this->_post = (object)$__post;

        $__get = $this->escape($_GET);
        $this->_get = (object)$__get;

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest') == 0) {
            $this->_ajax = true;
        }

        //Получаем список тем в папке layouts
        $this->_themes = themes();
        //Устанавливаем тему из конфига, если такая есть
        //Если нет такой темы и нет темы default - берем первую из вписка.
        $this->_default_layout = $this->_layout;
        if (isset($this->_config->layout) && isset($this->_themes[$this->_config->layout])){
            $this->_layout = $this->_config->layout;
        } elseif (!empty($this->_themes) && !isset($this->_themes[$this->_layout])){
            reset($this->_themes);
            $this->_layout = current($this->_themes);
        }

        Core::$app = $this;
    }

    public static function start($config = null)
    {
        if (!static::$__instance) {
            static::$__instance = new static($config);
        }
        return static::$__instance;
    }

    public function getDb()
    {
        if (!$this->_db && !empty($this->_config->db))
            $this->_db = new Db($this->_config->db);
        return $this->_db;
    }

    // Check on AJAX request
    public function isAjax()
    {
        return $this->ajax;
    }

    public function getAjax()
    {
        return $this->_ajax ?? false;
    }

    // Work with $_GET
    public function getIsGet()
    {
        return !empty($this->_get);
    }

    public function getGet()
    {
        return $this->_get;
    }

    // work with $_POST
    public function getIsPost()
    {
        return !empty($this->_post);
    }

    public function getPost()
    {
        return $this->_post;
    }

    // $_SESSION
    public function getSession()
    {
        if (empty($this->_session))
            $this->_session = new Session();
        return $this->_session;
    }

    // work with $_FILES
    public function getIsFiles()
    {
        return !empty($_FILES);
    }

    // App config
    public function getConfig()
    {
        return $this->_config;
    }

    /** Escape string with DB or without it
     * @param string|array
     * @return string|array|null
     */
    private function escape($element)
    {
        $result = null;
        if (is_array($element)) {
            foreach ($element as $key => $value)
                $result[$key] = $this->escape($value);
        } else {
            $result = htmlentities($element, ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5, "UTF-8");
        }
        return $result;
    }

    //перенести накуй в db
    private function noDbEscape($string)
    {
        $search = array("\\", "\x00", "\n", "\r", "'", '"', "\x1a");
        $replace = array("\\\\", "\\0", "\\n", "\\r", "\'", '\"', "\\Z");
        return str_replace($search, $replace, $string);
    }

    //ХЗ надо или нет, но пока оставляем
    private static function loader($class)
    {
        $cc = explode('\\', $class);
        array_shift($cc);

        $list = static::start()->config->directory_classes;
        if (false === array_search($cc[0], $list))
            return false;

        array_push($cc, 'index.php');
        $class = implode(DS, $cc);
        include ROOT_DIR . DS . $class;
        return true;
    }

    public function getPresenters()
    {
        if (empty($this->_presenters))
            $this->_presenters = new PresentersList();
        return $this->_presenters;
    }

    public function getLayout()
    {
        return $this->_layout;
    }

    public function getDefaultLayout()
    {
        return $this->_default_layout??$this->_layout;
    }

    static public function __callStatic($method, $args) {

        if (preg_match('/^([gs]et)([A-Z])(.*)$/', $method, $match)) {
            $reflector = new \ReflectionClass(__CLASS__);
            $property = strtolower($match[2]). $match[3];
            if ($reflector->hasProperty($property)) {
                $property = $reflector->getProperty($property);
                switch($match[1]) {
                    case 'get': return $property->getValue();
                    case 'set': return $property->setValue($args[0]);
                }
            } else throw new InvalidArgumentException("Property {$property} doesn't exist");
        }
    }
}