<?php

namespace avtokar\lightmvc;

use app\view;
use function avtokar\returnContent;

class Core
{
	/** @var Application */
	static public $app = null;

	private function __construct()
	{
	}

	static function route()
	{
		$routePath = static::getPath();

		if(preg_match('#\.js#',$routePath) || preg_match('#\.css#',$routePath)){
		    returnContent($routePath, Core::$app->layout, Core::$app->defaultLayout);
            return;
        }

		$viewName = 'app\\views\\'.ucfirst(strtolower($routePath));
		if (class_exists($viewName)) {
			$view = new $viewName;
		} else {
			header("HTTP/1.0 404 Not Found");
			return;
		}
		static::$app->presenters->run();
		return;
	}

	/**
	 * getPath()
	 *
	 * Парсер URI. Определяет controller/action для обработки
	 *
	 * @return string
	 */
	private static function getPath()
	{
		$request = explode('?', $_SERVER[ 'REQUEST_URI' ]);

		if (!empty($_SERVER[ 'QUERY_STRING' ]))
			array_pop($request);
		if (!empty($request))
			$request = array_shift($request);
		$request = rtrim(ltrim($request, '/'), '/');
		$routing = static::$app->config->route;
		$execPath = $request;

		if(preg_match('#\.js#',$execPath) || preg_match('#\.css#',$execPath)){
		    return $execPath;
        }

		foreach ($routing as $key => $value) {
			if (strtolower($request) === strtolower($key))
				return $value;

			$params = [];
			$pattern = "#^$key$#";
			preg_match_all('#<(\w+):(\w+)>#', $key, $matches);

			if (!empty($matches[ 0 ])) {
				foreach ($matches[ 0 ] as $mKey => $mValue) {
					$params[] = $matches[ 1 ][ $mKey ];
					$pattern = str_replace($mValue, "(\\" . $matches[ 2 ][ $mKey ] . "+)", $pattern);
				};
			}

			$matches = [];
			preg_match_all("#<\w+>#", $pattern, $matches);

			if (!empty($matches[ 0 ])) {
				foreach ($matches[ 0 ] as $mValue) {
					$pattern = str_replace($mValue, "(\\w+)", $pattern);
				};
			}

			preg_match($pattern, $request, $testRoute);
			if (!empty($testRoute)) {
				array_shift($testRoute);
				while (!empty($params)) {
					$pKey = array_pop($params);
					$pValue = array_pop($testRoute);
					static::$app->get->$pKey = $pValue;
				};

				while (!empty($matches[ 0 ])) {
					$fromParam = array_shift($matches[ 0 ]);
					$toParam = array_shift($testRoute);
					$value = str_replace($fromParam, $toParam, $value);
				};

				return $value;
			}
		}
		return "404";
	}

	public static final function __callStatic($name, $arguments = [])
	{
		if( isset( self::$$name)) {
			return self::$$name;
		} else {
		    return null;
		}
	}
}