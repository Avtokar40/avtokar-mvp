<?php

namespace avtokar\lightmvc;

class Db
{
	use Utils;
	/** @var \mysqli|null */
	private $connect = null;
	/** @var mixed */
	private $_result = null;
	/** @var string */
	private $_query = null;
	/** @var integer */
	private $_error = null;
	/** @var string */
	private $_errorMsg = null;

	public function __construct($params = [])
	{
		$connInfo = $this->fill([ 'host', 'dbase', 'user', 'pass' ], $params);

		if (empty($connInfo->host) || empty($connInfo->dbase) || empty($connInfo->user))
			return null;

		$this->connect = @mysqli_connect($connInfo->host, $connInfo->user, $connInfo->pass);
		if (!$this->connect) {
			$this->_error = mysqli_connect_errno();
			$this->_errorMsg = mysqli_connect_error();
			return null;
		}
		if (!$this->connect->select_db($connInfo->dbase)) {
			$this->_error = '-1';
			$this->_errorMsg = "database '{$connInfo->dbase}' not found on server";
			return null;
		}
	}

	/**
	 * @param $string
	 * @return string
	 */
	public function escape($string)
	{
		return $this->connect->escape_string($string);
	}

	public function columns($table)
	{
		$this->_query = "SHOW COLUMNS FROM `$table`";
		$res = $this->query();
		if ($res)
			return $res->fetch_all(MYSQLI_ASSOC);
		return [];
	}

	public function keys($table)
	{
		$this->_query = "SHOW KEYS FROM `$table`";
		$res = $this->query();
		if ($res)
			return $res->fetch_all(MYSQLI_ASSOC);
		return [];
	}

	public function select($params = [])
	{
		$fields = '*';
		$condition = "";
		$group = "";
		$order = "";
		$limit = "";
		$join = "";

		if (empty($params) || empty($params[ 'table' ]))
			return null;
		$table = $params[ 'table' ];

		if (!empty($params[ 'fields' ])) {
			$fields = $params[ 'fields' ];
			if (is_array($fields))
				$fields = implode(', ', $fields);
		};

		if (!empty($params[ 'group' ])) {
			$group = $params[ 'group' ];
			if (is_array($group)) {
				$order = implode(', ', $group);
			}
			$order = "ORDER BY " . $order;
		};

		if (!empty($params[ 'join' ])) {
			$join = $params[ 'join' ];
			if (is_array($join)) {
				$table2 = $join[ 'table' ];
				$relation = $join[ 'relation' ];
				if (is_array($relation)) {
					$relTo = reset($relation);
					$relFrom = array_search($relTo, $relation);
					$relation = "$table.$relFrom = $table2.$relTo";
				}
				$join = "JOIN $table2 ON $relation";
			}
		};

		if (!empty($params[ 'order' ])) {
			$order = $params[ 'order' ];
			if (is_array($order)) {
				$tmp = [];
				foreach ($order as $key => $value)
					$tmp[] = "$key $value";
				$order = implode(', ', $tmp);
			}
			$order = "ORDER BY " . $order;
		};

		if (!empty($params[ 'limit' ])) {
			$limit = $params[ 'limit' ];
			if (is_array($limit)) {
				$offset = null;
				$count = null;
				if (isset($limit[ 'offset' ])) {
					$offset = $limit[ 'offset' ];
					unset($limit[ 'offset' ]);
				};
				if (isset($limit[ 'count' ])) {
					$count = $limit[ 'count' ];
					unset($limit[ 'count' ]);
				};

				if (!$offset && !empty($limit))
					$offset = array_shift($limit);
				if (!$count && !empty($limit))
					$count = array_shift($limit);

				$limit = $offset ? "$offset, " : '';
				$limit .= isset($count) ? $count : '';
			}
			$limit = "LIMIT " . $limit;
		};

		if (!empty($params[ 'condition' ])) {
			$condition = $params[ 'condition' ];
			if (is_array($condition)) {
				$condition = $this->buildWhere($condition);
			};
			$condition = "WHERE " . $condition;
		};

		$this->_query = str_replace("  ", " ", "SELECT $fields FROM $table $join $condition $group $order $limit");
		return $this->query();
	}

	public function put($table, $params, $insert)
	{
		$condition = "";
		if (!$insert) {
			$condition = "WHERE " . $params[ "PRIMARY_KEY" ][ 0 ] . " = " . $params[ "PRIMARY_KEY" ][ 1 ];
			unset ($params[ "PRIMARY_KEY" ]);
		};
		$data = [];
		foreach ($params as $key => $value) {
			if (!(is_int($value) || is_float($value)))
				$value = "'$value'";
			$data[] = "$key = $value";
		};
		$data = implode(', ', $data);
		$header = $insert ? "INSERT INTO" : "UPDATE";
		$this->_query = "$header $table SET $data $condition";
		return $this->query();
	}

	public function delete($table, $field, $value)
	{
		if (!(is_int($value) || is_float($value)))
			$value = "'$value'";
		$this->_query = "DELETE FROM $table WHERE $field = $value";
		return $this->query();
	}

	public function insertId()
	{
		return $this->connect->insert_id;
	}

	private function query($query = null)
	{
		if (!$query)
			$query = $this->_query;
		$result = $this->connect->query($query);
		return $result;
	}

	/**
	 * @param $where array [$action, $firstArgument, $secondArgument]
	 * @return string
	 */
	private function buildWhere($where)
	{
		$result = "";
		$action = array_shift($where);
		$first = array_shift($where);
		$second = array_shift($where);
		if ($action == 'and' || $action == "or") {
			$result = "( " . $this->buildWhere($first) . ") $action (" . $this->buildWhere($second) . ")";
		} else {
			if (!(is_int($second) || is_float($second)))
				$second = "'$second'";
			$result = "$first $action $second";
		}
		return $result;
	}
}