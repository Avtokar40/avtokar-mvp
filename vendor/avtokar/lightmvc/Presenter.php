<?php

namespace avtokar\lightmvc;

use avtokar\Component;

class Presenter extends Component
{
    protected $dependences = [];
    protected $_listeners = null;
    protected $_data = null;

    final function add($listener, $data)
    {
        $this->_listeners[$listener] = $data;
    }

    public function __construct()
    {
        foreach ($this->dependences as $presenter => $data) {
            Core::$app->presenters->$presenter->add('pres_'.$this::_class(), $data);
        };
    }

    function refresh()
    {
        foreach ($this->_listeners as $listener => $dataList){
            $curr = Core::$app->views[$listener];
            foreach ($dataList as $data){
                if(isset($this->_data[$data])){
                    $curr->$data = $this->_data[$data];
                }
            }
        }
    }

    function getDependences()
    {
        return $this->dependences;
    }

    function run()
    {

    }
}