<?php
/**
 * Created by PhpStorm.
 * User: Vladimir
 * Date: 01.01.2019
 * Time: 13:25
 */

namespace avtokar\lightmvc;

class PresentersList extends \avtokar\Component
{
    private $_list = null;

    public function __get($name)
    {
        if (empty($this->_list["$name"])) {
            $class = 'app\\presenter\\' . $name;
            if (class_exists($class)) {
                $this->_list["$name"] = new $class;
            } else {
                $this->_list["$name"] = new \avtokar\lightmvc\Presenter();
            }
        }
        return $this->_list["$name"];
    }

    private function listSort()
    {
        $list = [];
        foreach ($this->_list as $id => $item) {
            $list[$id] = ['index' => 0, 'deps' => array_keys($item->dependences)];
        };
        $sorted = [];
        reset($list);
        while (!empty($list)) {
            $key = key($list);
            if (empty($list[$key]['deps'])) {
                $sorted[$key] = $list[$key]['index'];
                unset($list[$key]);
                continue;
            }
            foreach ($list[$key]['deps'] as $index => $value) {
                if (isset($sorted[$value])) {
                    $list[$key]['index'] = $list[$key]['index'] > $sorted[$value] ? $list[$key]['index'] : $sorted[$value] + 1;
                    unset($list[$key]['deps'][$index]);
                } else {
                    if (false === next($list)){
                        reset($list);
                    }
                }
            }
        }
        asort($sorted);
        foreach (array_keys($sorted) as $key){
            $list[$key] = $this->_list[$key];
        }
        $this->_list = $list;
    }

    public function run()
    {
        $this->listSort();

//        foreach ($this->_list as $item) {
//            $item->run();
//            $item->refresh();
//        }
    }
}