<?php
/**
 * Created by PhpStorm.
 * User: Vladimir
 * Date: 01.01.2019
 * Time: 13:25
 */

namespace avtokar\lightmvc;

use avtokar\Component;

class Session extends Component
{
    public function __construct()
    {
        session_start();
    }

    public function __get($name)
    {
        if (!empty($_SESSION[$name])) {
            return $_SESSION[$name];
        }
        $name = ucfirst($name);
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }
        return null;
    }

    public function __set($name, $value)
    {
        $setter = 'set' . ucfirst($name);
        if (method_exists($this, $setter)) {
            return $this->$setter($value);
        }
        return $_SESSION[$name] = $value;
    }

    public function __unset($name)
    {
        if (isset($_SESSION[$name]))
            unset($_SESSION[$name]);
        return null;
    }

    public function __isset($name)
    {
        return isset($_SESSION[$name]);
    }

    public function flash($name)
    {
        $value = null;
        if (isset($_SESSION[$name])) {
            $value = $_SESSION[$name];
            unset($_SESSION[$name]);
        }
        return $value;
    }
}