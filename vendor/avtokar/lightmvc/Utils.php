<?php
/**
 * Created by PhpStorm.
 * User: Vladimir
 * Date: 01.02.2019
 * Time: 10:27
 */

namespace avtokar\lightmvc;

trait Utils
{
	/**
	 * Заполнение объекта с полями names значениями values
	 * поиск по именам и/или порядке параметров
	 *
	 * @param array $names
	 * @param $values
	 * @return object
	 */
	function fill($names = [], $values)
	{
		$values = (array)$values;
		$result = (object)[];
		foreach ($names as $id => $name) {
			if (isset($values[ $name ])) {
				$result->$name = $values[ $name ];
				unset ($values[ $name ]);
				unset ($names[ $id ]);
			}
		}
		foreach ($names as $name) {
			$result->$name = array_shift($values);
		}
		return $result;
	}
}