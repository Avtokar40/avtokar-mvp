<?php

namespace avtokar\lightmvc;

use avtokar\Component;
use app\views;

class View extends Component
{
    protected $data = [];
    protected $presenters = [];
    protected $layout = 'index';

    public function __construct()
    {
        foreach ($this->presenters as $presenter => $data) {
            Core::$app->presenters->$presenter->add($this::_class(), $data);
        };
    }

    public function __get($name)
    {
        if (isset($this->data[$name])) {
            return $this->data[$name];
        }
        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        return $this->data[$name] = $value;
    }

    public function render()
    {
    }
}