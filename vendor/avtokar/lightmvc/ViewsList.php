<?php
/**
 * Created by PhpStorm.
 * User: Vladimir
 * Date: 01.01.2019
 * Time: 13:25
 */

namespace avtokar\lightmvc;

use avtokar\Component;
use app\presenter;

class ViewsList extends Component
{
    private $_list = null;

    public function __get($name)
    {
        if (empty($this->_list["$name"])) {
            $class = 'app\\presenter\\'.$name;
            if(class_exists($class)){
                $this->_list["$name"] = new $class;
            } else {
                $this->_list["$name"] = new \avtokar\lightmvc\Presenter();
            }
        }
        return $this->_list["$name"];
    }

    public function __set($name, $value)
    {
        $setter = 'set' . ucfirst($name);
        if (method_exists($this, $setter)) {
            return $this->$setter($value);
        }
        return $_SESSION[$name] = $value;
    }

    public function __unset($name)
    {
        if (isset($_SESSION[$name]))
            unset($_SESSION[$name]);
        return null;
    }

    public function __isset($name)
    {
        return isset($_SESSION[$name]);
    }

}